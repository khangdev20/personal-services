enum AssetLinks {
    AVATAR_PNG = "https://ik.imagekit.io/xzan1wy02b/IMG_8774.png",
    AVATAR = "https://ik.imagekit.io/xzan1wy02b/IMG_8774.JPG",
    LOGO_WHITE = "https://ik.imagekit.io/xzan1wy02b/1-cropped.svg",
    LOGO_BLACK = "https://ik.imagekit.io/xzan1wy02b/Modern%20Minimalist%20Graffiti%20Dream%20Brand%20Logo-cropped.svg",
}

enum ESocialIconLinks {
    FACEBOOK = "https://upload.wikimedia.org/wikipedia/en/0/04/Facebook_f_logo_%282021%29.svg",
    INSTAGRAM = "https://upload.wikimedia.org/wikipedia/commons/e/e7/Instagram_logo_2016.svg",
    LINKEDIN = "https://upload.wikimedia.org/wikipedia/commons/8/81/LinkedIn_icon.svg",
    GITHUB = "https://upload.wikimedia.org/wikipedia/commons/9/91/Octicons-mark-github.svg",
    GITLAB = "https://upload.wikimedia.org/wikipedia/commons/3/35/GitLab_icon.svg",
}
enum ESocialLinks {
    FACEBOOK = "https://www.facebook.com/vlnk.2512",
    INSTAGRAM = "https://www.instagram.com/vlnk_2512",
    LINKEDIN = "https://www.linkedin.com/in/vokhang",
    GITHUB = "https://github.com/khangdev20",
    GITLAB = "https://gitlab.com/khangdev20",
    FACEBOOK_PAGE = "https://www.facebook.com/it.codenerd",
}

export { AssetLinks, ESocialIconLinks, ESocialLinks };
