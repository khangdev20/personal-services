import React, { ReactNode } from 'react';
import Header from '../commons/Header';
import Footer from '../commons/Footer';
import { cn } from "@/libs/utils";

type LayoutDefault = {
    children: ReactNode
}

const LayoutDefault = ({ children }: LayoutDefault) => {
    return (
        <section>
            <Header />
            {children}
            <Footer />
        </section>
    )
}

export default LayoutDefault