import React, { ReactNode } from 'react';
import { cn } from "@/libs/utils";

type LayoutContentProps = {
    children: ReactNode
}

const LayoutContent = ({ children }: LayoutContentProps) => {
    return (
        <section
            className={cn("flex justify-center md:mx-0")}
        >
            <div
                className={cn("w-[1200px] m-5 xl:m-2")}
            >
                {children}
            </div>
        </section>
    )
}

export default LayoutContent