"use client"
import React, { Fragment, ReactNode } from 'react';
import Header from '../commons/Header';
import { usePathname } from '@/hooks/usePathname';
import LayoutDefault from './LayoutDefault';
import LayoutContent from './LayoutContent';

type LayoutProviderProps = {
    children: ReactNode
}

const LayoutProvider = ({ children }: LayoutProviderProps) => {
    const pathname = usePathname();
    const layoutConfigs = [
        {
            paths: [
                "/not-found"
            ],
            layout: null,
            content: null
        },
        {
            paths: [
                "/",
                "/pages/services",
                "/pages/blog",
                "/pages/Q&A",
                "/pages/contact",
                "/pages/documents"
            ],
            layout: LayoutDefault,
            content: LayoutContent
        }
    ];

    return (
        <div>
            {
                (() => {
                    const layout = layoutConfigs.find(x => x.paths.includes(pathname));
                    if (layout !== null) {
                        let Layout = layout?.layout ?? Fragment;
                        let Content = layout?.content ?? Fragment;
                        return (
                            <Layout>
                                <Content>
                                    {children}
                                </Content>
                            </Layout>
                        );
                    }
                    return (
                        <Fragment>
                            {children}
                        </Fragment>
                    )
                })()
            }
        </div>
    )
}

export default LayoutProvider;