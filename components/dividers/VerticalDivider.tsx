import { cn } from '@/libs/utils';
import React, { HTMLAttributes } from 'react';

type VerticalDivider = HTMLAttributes<HTMLDivElement> & {
}

const VerticalDivider = ({ className, ...props }: VerticalDivider) => {
    return (
        <div
            {...props}
            className={cn(
                "h-full min-w-1 max-w-2 bg-black min-h-5",
                className
            )}
        >
        </div>
    )
}

export default VerticalDivider;