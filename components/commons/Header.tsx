"use client"
import Link from "next/link";
import { cn } from "@/libs/utils";
import useScrollY from "@/hooks/useScrollY";
import { FiHome, FiUser } from "react-icons/fi";
import { ESocialIconLinks } from "@/constants";
import Brand from "./Brand";

type HeaderProps = {
}

export const headerItems = [
    {
        title: "Documents",
        path: "/pages/documents"
    },
    {
        title: "Services",
        path: "/pages/services"
    },
    {
        title: "Blog",
        path: "/pages/blog"
    },
    {
        title: "Contact",
        path: "/pages/contact"
    },
    {
        title: "Q&A",
        path: "/pages/Q&A"
    }
];

const Index = (props: HeaderProps) => {
    const scrollY = useScrollY();
    return (
        <div
            className={
                cn(
                    "md:h-16 h-14 items-center flex justify-between",
                    "gap-5 mx-2 px-5 md:px-12 md:mx-10",
                    "sticky top-0 transition-all duration-700 z-10 rounded-full",
                    {
                        "bg-black/10 backdrop-blur-sm top-2": scrollY > 20
                    }
                )
            }
        >
            <div className={cn("flex items-center gap-5")}>
                <Brand />
                <div
                    className={cn("hidden xl:flex items-center")}
                >
                    {headerItems.map((item, index) => (
                        <Link
                            key={index}
                            href={item.path}
                            className={cn(
                                "text-black hover:text-black/60 transition duration-200 text-base cursor-pointer",
                                "font-medium capitalize mx-10 font-inter tracking-widest underline-offset-2"
                            )}
                        >
                            {item.title}
                        </Link>
                    )
                    )}
                </div>
            </div>
            <FiUser className={"text-2xl"} />
        </div>
    )
}

export default Index;