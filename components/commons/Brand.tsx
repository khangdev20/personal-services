import React from 'react';
import Image from 'next/image';
import { AssetLinks } from '@/constants';
import { cn } from '@/libs/utils';
import Link from 'next/link';

const Brand = () => {
    return (
        <Link href="/">
            <Image className={cn("w-28 md:w-32")} src={AssetLinks.LOGO_BLACK} alt={"brand-logo"} height={200} width={200} />
        </Link>
    )
}

export default Brand;