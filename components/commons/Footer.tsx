"use client";
import { cn } from '@/libs/utils'
import React from 'react'
import { headerItems } from './Header'
import Link from 'next/link';
import Brand from './Brand';
import { AssetLinks, ESocialLinks } from '@/constants';
import { FiMail, FiPhone, FiMap } from "react-icons/fi";

type FooterProps = {
}

const Footer = (props: FooterProps) => {

    const contactInfo = [
        {
            content: "+84123456790",
            target: "tel",
            icon: FiPhone
        },
        {
            content: "khangne@gmail.com",
            target: "mailto",
            icon: FiMail
        },
        {
            content: "123, Dien Bien Phu Street, HCM City",
            target: "mailto",
            icon: FiMap
        }
    ]



    return (
        <section
            id='footer'
            className={cn(
                "w-full flex justify-center"
            )}
        >
            <div
                className={cn(
                    "w-[1200px] m-5 xl:m-2"
                )}
            >
                <div className={cn("flex items-start flex-col md:flex-row justify-between gap-10 my-5")}>
                    <div className={cn("col-span-1 flex flex-col gap-2")}>
                        <Brand />
                        <span className={"text-wrap"}>
                        </span>
                    </div>
                    <div className={cn("col-span-1 items-start flex flex-col gap-2")}>
                        <div className={cn("text-xl font-bold mb-5")}>Contact Info</div>
                        {contactInfo.map((item, index) => {
                            const Icon = item.icon;
                            return (
                                <div key={index} className={cn("flex items-center gap-2 font-medium")}>
                                    <Icon className='text-xl' />
                                    <a href={`${item.target}:${item.content}`} >
                                        {item.content}
                                    </a>
                                </div>
                            )
                        })}
                    </div>
                    <div className={cn("col-span-1 items-start flex flex-col gap-2")}>
                        <div className={cn("text-xl font-bold mb-5")}>Information</div>
                        {headerItems.map((item, index) => (
                            <Link
                                key={index}
                                href={item.path}
                                className={cn(
                                    "text-black hover:text-black/60 transition duration-200 text-base cursor-pointer",
                                    "font-medium capitalize tracking-widest underline-offset-2"
                                )}
                            >
                                {item.title}
                            </Link>
                        ))}
                    </div>
                </div>
            </div>
        </section>
    )
}

export default Footer