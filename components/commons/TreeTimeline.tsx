import React, { HTMLAttributes } from 'react';
import { cn } from '@/libs/utils';

type TreeTimelineProps = HTMLAttributes<HTMLDivElement> & {
    date?: string,
    endDate?: string,
    index: number,
    descriptions?: string[],
    title?: string,
    subTitle?: string,
    lenght?: number,
    position: number,
}

const TreeTimeline = ({
    position,
    date,
    index,
    title,
    subTitle,
    descriptions,
    className,
    lenght,
    ...props
}: TreeTimelineProps) => {
    return (
        <div className="relative pl-8 sm:pl-32 py-6 group">
            <div className={cn(
                "flex flex-col sm:flex-row items-start mb-1 group-last:before:hidden before:absolute before:left-2 sm:before:left-0 before:h-full before:px-px before:bg-slate-300 sm:before:ml-[6.5rem] before:self-start before:-translate-x-1/2 before:translate-y-3 after:absolute after:left-2 sm:after:left-0 after:border-4 after:box-content after:border-slate-50 after:rounded-full sm:after:ml-[6.5rem] after:-translate-x-1/2 after:translate-y-1.5",
                {
                    "after:bg-red-600 after:w-3 after:h-3": index + 1 === lenght,
                    "after:bg-indigo-600 after:w-2 after:h-2": !(index + 1 === lenght)
                }
            )}>
                <div
                    className={cn(
                        "gap-1 text-emerald-600 sm:absolute left-5 translate-y-0.5 text-xs font-semibold uppercase h-6 mb-3 sm:mb-0 inline-flex items-center justify-center"
                    )}
                >
                    <div className={cn("flex flex-col gap-1")}>
                        <div className="p-1 px-2 bg-emerald-100 rounded-full">
                            {date}
                        </div>
                    </div>
                </div>
                <div>
                    <div
                        className={cn(
                            "text-xl md:text-2xl font-bold text-slate-900 font-caveat",
                            {
                                "hidden": !title
                            }
                        )}
                    >
                        {title}
                    </div>
                    <div
                        className={cn(
                            "text-base xl:text-xl font-bold text-slate-900",
                            {
                                "hidden": !subTitle
                            }
                        )}
                    >
                        {subTitle}
                    </div>
                    <div>
                        {
                            descriptions !== null && descriptions?.map((item, index) => {
                                return (
                                    <li className={cn(
                                        "text-slate-500 list-disc",
                                        className
                                    )} key={index}>
                                        {item}
                                    </li>
                                )
                            })
                        }
                    </div>
                </div>
            </div>
        </div>
    )
}

export default TreeTimeline;