import type { Metadata } from "next";
import "./globals.css";
import React from "react";
import LayoutProvider from "@/components/layouts/LayoutProvider";


export const metadata: Metadata = {
    title: "Vo Le Nhut Khang - Portfolio",
};

export default function RootLayout({
    children,
}: Readonly<{
    children: React.ReactNode;
}>) {
    return (
        <html lang="en">
            <body>
                <LayoutProvider>
                    {children}
                </LayoutProvider>
            </body>
        </html>
    );
}
