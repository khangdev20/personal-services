import TreeTimeline from "@/components/commons/TreeTimeline";
import { AssetLinks, ESocialIconLinks, ESocialLinks } from "@/constants";
import { cn } from "@/libs/utils";
import Image from "next/image";
import Link from "next/link";


export default function Home() {
    const projects = [
        {
            date: "Apr 2023",
            endDate: "Nov 2023",
            descriptions: [
            ],
            title: "Backend Virtual Run Application",
            subTitle: "at Estuary JSC",
        },
        {
            date: "Apr 2023",
            endDate: "Nov 2023",
            descriptions: [
            ],
            descriptionStyles: "list-none text-base text-black",
            title: "Backend Lucky Draw Wesbite",
            subTitle: "at Estuary JSC",
        },
        {
            date: "Apr 2023",
            endDate: "Nov 2023",
            descriptions: [
            ],
            title: "Backend Virtual Sport Application",
            subTitle: "at Estuary JSC",
        },
        {
            date: "Apr 2023",
            endDate: "Nov 2023",
            descriptions: [
            ],
            title: "Movie Website",
            subTitle: "at Ho Chi Minh City University of Technology (HUTECH)",
        },
        {
            date: "Apr 2023",
            endDate: "Nov 2023",
            descriptions: [
            ],
            title: "E-Commerece Website",
            subTitle: "at Ho Chi Minh City University of Technology (HUTECH)",
        },
        {
            date: "Apr 2023",
            endDate: "Nov 2023",
            descriptions: [
            ],
            title: "Music Website",
            subTitle: "at Ho Chi Minh City University of Technology (HUTECH)",
        }
    ]

    const socials = [
        {
            icon: ESocialIconLinks.FACEBOOK,
            url: ESocialLinks.FACEBOOK
        },
        {
            icon: ESocialIconLinks.INSTAGRAM,
            url: ESocialLinks.INSTAGRAM
        },
        {
            icon: ESocialIconLinks.LINKEDIN,
            url: ESocialLinks.LINKEDIN
        },
        {
            icon: ESocialIconLinks.GITHUB,
            url: ESocialLinks.GITHUB
        },
        {
            icon: ESocialIconLinks.GITLAB,
            url: ESocialLinks.GITLAB
        }
    ]

    return (
        <div className={"w-full flex justify-center"}>
            <div className={"flex flex-col gap-10"}>
                <div className={"flex justify-center items-center md:flex-row flex-col-reverse tracking-wide md:items-end gap-10"}>
                    <div className={"flex flex-col gap-3 items-start"}>
                        <span
                            className={
                                cn(
                                    "font-alex-brush text-[70px]",
                                    "md:flex hidden translate-y-4"
                                )
                            }
                        >
                            Hi There!
                        </span>
                        <span className={"font-tinos md:text-[60px] text-[40px] font-bold"}>
                            {"I'm Le Nhut Khang, VO"}
                        </span>
                        <span
                            className={cn(
                                "font-inter font-bold",
                                "md:text-[20px] text-[15px]"
                            )}
                        >
                            Software Developer, UI/UX Designer,...
                        </span>
                    </div>
                    <div
                        className={cn(
                            "shadow-xl p-5 rounded-2xl outline-1 outline outline-gray-300"
                        )}
                    >
                        <Image
                            className={cn(
                                "object-cover object-center rounded-xl",
                                "h-[400px] w-[400px] mb-5"
                            )}
                            src={AssetLinks.AVATAR}
                            alt={"my-portpolio"}
                            height={650}
                            width={650}
                        />
                        <div className={cn("flex justify-end items-center gap-2")}>
                            {socials.map((item, index) => (
                                <Link key={index} href={item.url} target="_blank">
                                    <Image src={item.icon} alt={item.icon} height={25} width={25} />
                                </Link>
                            ))}
                        </div>
                    </div>
                </div>
                <div className={'w-full bg-accent p-[0.5px] rounded-full'}></div>
                <div className={"gap-5 flex flex-col "}>
                    <div className="flex gap-5 my-5">
                        <button className={"font-medium trasition duration-300 h-[50px] bg-black text-primary rounded-full px-10 hover:scale-105"}>
                            Talk with me
                        </button>
                        <Link href={ESocialLinks.FACEBOOK_PAGE} target="_blank">
                            <button className={"font-medium flex items-center trasition duration-300 h-[50px] bg-primary text-black border-accent border rounded-full px-10 hover:scale-105"}>
                                Join Us
                            </button>
                        </Link>
                    </div>
                    <div>
                        {projects.map((item, index) => {
                            return (
                                <TreeTimeline
                                    key={index}
                                    index={index}
                                    position={index}
                                    title={item.title}
                                    className={"list-none"}
                                    endDate={item.endDate}
                                    lenght={projects.length}
                                    subTitle={item.subTitle}
                                    date={item.date}
                                    descriptions={item.descriptions}
                                />
                            )
                        })}
                    </div>
                </div>
            </div>
        </div >
    );
}
