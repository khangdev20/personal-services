/** @type {import('next').NextConfig} */
const nextConfig = {
    distDir: process.env.NODE_ENV === "production" ? "build" : undefined,
    output: process.env.NODE_ENV === "production" ? "export" : undefined,
    assetPrefix: process.env.NODE_ENV === "production" ? "/" : undefined,
    images: {
        unoptimized: true,
        remotePatterns: [
            {
                protocol: "https",
                hostname: "ik.imagekit.io",
            },
            {
                protocol: "https",
                hostname: "res.cloudinary.com",
            },
        ],
    },
};

export default nextConfig;
