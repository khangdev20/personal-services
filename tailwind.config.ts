import type { Config } from "tailwindcss";

const config: Config = {
    content: [
        "./pages/**/*.{js,ts,jsx,tsx,mdx}",
        "./components/**/*.{js,ts,jsx,tsx,mdx}",
        "./app/**/*.{js,ts,jsx,tsx,mdx}",
    ],
    theme: {
        extend: {
            colors: {
                primary: "#FFFBF5",
                secondary: "#3A3A3A",
                accent: "#BDBDBD",
                black: "#3A3A3A",
            },
            fontFamily: {
                tinos: ["Tinos", "sans-serif"],
                "alex-brush": ["Alex Brush", "sans-serif"],
                inter: ["Inter", "sans-serif"],
                caveat: ["Caveat", "sans-serif"],
            },
        },
    },
    plugins: [],
};
export default config;
