import React from "react";
import { usePathname as useNextPathname, useParams } from "next/navigation";

const usePathname = () => {
    const pathname = useNextPathname();
    const slug = useParams();
    let rootSlug = pathname;
    if (slug != null) {
        const keys = Object.keys(slug);
        for (const key of keys) {
            if (slug[key] != null) {
                rootSlug = rootSlug.replace(slug[key].toString(), `[${key}]`);
            }
        }
    }
    return rootSlug;
};

export { usePathname };
