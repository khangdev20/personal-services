import { useEffect, useState } from "react";

const useScrollY = () => {
    const [scrollY, setScrollY] = useState(0);
    useEffect(() => {
        if (window !== undefined) {
            setScrollY(window.scrollY);
        }
        const onScroll = () => {
            setScrollY(window.scrollY);
        };
        window.addEventListener('scroll', onScroll);
        return () => {
            window.removeEventListener('scroll', onScroll);
        };
    }, []);
    return scrollY;
};
export default useScrollY;